# Get base OS from Ubunut
FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
# Update system
RUN apt update

# Install sudo for installing other packages
RUN apt install -y sudo

# Install below yocto system requirements for buiding yocto on 18.04 OS
RUN apt install -y gawk wget git diffstat unzip texinfo gcc build-essential chrpath socat \
    cpio python3 python3-pip python3-pexpect xz-utils debianutils iputils-ping python3-git \
    python3-jinja2 libegl1-mesa libsdl1.2-dev python3-subunit mesa-common-dev zstd \
    liblz4-tool file locales libacl1

# Setup locales
RUN locale-gen en_US.UTF-8
RUN update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Install curl for downloading the repo
RUN apt install -y curl

# Run below command to download the opensource repo
RUN curl https://storage.googleapis.com/git-repo-downloads/repo -o /usr/local/bin/repo

# Run below command to make repo executable for all users
RUN chmod a+x /usr/local/bin/repo

# Export repo path to system path variable
RUN export PATH=/usr/local/bin:$PATH

# Install vim file editor inside the container
RUN apt install -y vim

# Change python version system-wide with update-alternatives python
# 20.04 default python version is 3.6
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.6 1

# Change your /bin/sh symlink to point to bash yocto build environment needed bash
RUN ln -sf /bin/bash /bin/sh

# Export UID and GID in container
ARG USER
ARG WORKDIR
ARG UID
ARG GID
RUN groupadd -g $GID -o $USER
RUN useradd -m -u $UID -g $GID -o -s /bin/bash $USER

WORKDIR $WORKDIR

USER $USER
# Run below commands if you haven't setup gitconfig
RUN git config --global user.email $USER@example.com
RUN git config --global user.name $USER
RUN git config --global color.ui auto
