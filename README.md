## QCOM docker README

If you already installed the docker please skip below docker installation steps

### Install docker Using the installation script

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
```

### Run the docker.sh script

```bash
sudo sh get-docker.sh
```

### Setting docker Engine permission on Ubuntu

```bash
sudo groupadd docker
sudo usermod -aG docker $USER
```

### set permission to the docker.sock file

```bash
sudo chmod 666 /var/run/docker.sock
```

### Try running below test command to verify docker

```bash
docker run hello-world
```

### Update your giconfig user name and email in Docker file if you don't have your account identity in ~/.gitconfig
### If you are planning to use HOME directory ~/.gitconfig please comment below two lines from docker file

```bash
RUN git config --global user.email $USER@example.com
RUN git config --global user.name $USER
```

### Set release env variables
Supported Dockerfiles ( Dockerfile_20.04 )
To download the qcom-6.6.00-QLI.1.0-Ver.1.0 release with Dockerfile_20.04

```bash
QCOM_RELEASE=qcom-6.6.00-QLI.1.0-Ver.1.0
DOCKER_FILE=Dockerfile_20.04
source ${QCOM_RELEASE}/config.sh ${DOCKER_FILE}
```

### Docker help commands

| Docker help Commands | Description |
| ------- | ----------- |
| `--file OR -f` | Name of the Dockerfile (Default is PATH/Dockerfile) |
| `--build-arg` | Set build-time variables |
| `--no-cache` | Do not use cache when building the image ( If you face Fetch errors try this ) |
| `--rm` | Automatically remove the container when it exits |
| `-i, --interactive` | Keep STDIN open even if not attached |
| `-t, --tty` | Allocate a pseudo-TTY |
| `--volume OR -v` | Bind mount a volume |
| `--user OR -u` | Username or UID (format: <name|uid>[:<group|gid>]) |

### Create qcom release yocto docker image

```bash
docker build -f $DOCKER_FILE \
    --build-arg "USER=$(whoami)" \
    --build-arg "UID=$(id -u)" \
    --build-arg "GID=$(id -g)" \
    --build-arg "WORKDIR=$WORK_DIR" \
    --no-cache -t "$DOCKER_TAG" .
```

### If you see E: Unable to fetch some archives, maybe run apt-get update or try with --fix-missing?
### while docker build try below commands and try again docker build command

```bash
sudo apt clean
sudo apt update
sudo apt install --fix-missing
```

### Build qcom release in a docker container

```bash
docker run --rm -it -v ${HOME}:/home/${USER} \
    -v $(pwd):$(pwd) \
    -u $(id -u):$(id -g) \
    "$DOCKER_TAG" \
    /bin/bash ./sync_build.sh \
    -u "$URL" \
    -b "$BRANCH" \
    -m "$MANIFEST_FILE" \
    -M "$MACHINE" \
    -d "$DISTRO" \
    -i "$IMAGE" \
    -w "$WORK_DIR/$DOCKER_TAG"
```
